/* This is constant class which contains contstants related to watson */

/* This is a watson constants file*/

public class WatsonConstants {

    public static final Integer SUCCESS_RESPONSE_STATUS_CODE = 200; 
   
    public static final String WATSON_AUTHORIZATION_TYPE_BASIC = 'Basic ';
    
    public static final String WATSON_SET_AUTHORIZATION_HEADER_KEY = 'Authorization';
    
    public static final String WATSON_HTTP_ACTION_GET = 'GET';
    
    public static final String WATSON_ANSWER_MAP_KEY_TOP_CLASS = 'top_class'; 
    
    public static final String WATSON_ANSWER_MAP_KEY_CLASSES = 'classes';
    
    public static final String WATSON_ANSWER_MAP_KEY_CONFIDENCE = 'confidence';
    
    public static final String WATSON_CALLOUT_ERROR_MESSAGE = 'Some error has occured while connecting to Watson. Please try later.'; 
    
    public static final String WATSON_QUERY_PARAM_TEXT = 'text';
    
}